#include <stdio.h>
#include <iostream>
#include <string>
#include <vector>

#ifndef _READ_DATA_
#define _READ_DATA_

struct records {
	char opr;
	int reg1;
	int reg2;
	int reg3;
};

std::vector <records> argumentCollectionPhase();

#endif