#include <stdio.h>
#include <iostream>
#include <fstream>
#include <string>
#include <vector>

#include"ReadData.h"


std::vector <records> argumentCollectionPhase(){
	
	std::vector<records> inData;
	std::string buf;
	const char* chStr;
	records bufferData;

	std::fstream file;
	file.open("RandomRegisters.txt", std::ios::in);
	
	
	if (!file.good()) {
		std::cerr << " can't find the file ";
	}

	while (std::getline(file, buf)) {
		static int i = 0;
		chStr = buf.c_str();
		std::sscanf(chStr, "%s R%d R%d R%d", &bufferData.opr, &bufferData.reg1, &bufferData.reg2, &bufferData.reg3);
	//	std::cout << bufferData.opr << " " << bufferData.reg1 << " " << bufferData.reg2 << " " << bufferData.reg3 << std::endl;
		inData.push_back(bufferData);
		i++;
	}
	
	file.close();
	return inData;
}

